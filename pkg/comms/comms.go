package comms

import (
	"fmt"
	"os"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

//Ultrasonic deals with pinging an ultrasonic sensor connected the the rraspberry pi
//Currently only supports sr04 and maybe sr06
type Ultrasonic struct {
	TriggerPin rpio.Pin
	EchoPin    rpio.Pin
}

//NewUltrasonic creates a new instance of the ulstrasonic class
func NewUltrasonic(triggerPin int, echoPin int) *Ultrasonic {
	s := new(Ultrasonic)
	s.TriggerPin = rpio.Pin(triggerPin)
	s.EchoPin = rpio.Pin(echoPin)
	return s
}

//Initialize opens the Raspberry Pi GPIO interface for use
func (u *Ultrasonic) Initialize() {
	// Open and map memory to access gpio, check for errors
	if err := rpio.Open(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	u.TriggerPin.Output()
	u.EchoPin.Input()
	u.EchoPin.PullUp()
	u.EchoPin.Detect(rpio.AnyEdge) // enable falling edge event detection
}

//Measure performas a distance measurement
func (u *Ultrasonic) Measure() int64 {
	u.sendTrigger()
	echoTime := u.listenForEcho()

	//Formulat to covert to distance using speed of sound
	distance := (echoTime.Milliseconds() * 34300) / 2
	return distance
}

func (u *Ultrasonic) sendTrigger() {

	u.TriggerPin.High()
	time.Sleep(time.Microsecond * 10)
	u.TriggerPin.Low()

}

func (u *Ultrasonic) listenForEcho() time.Duration {

	var startTime, endTime time.Time
	if u.EchoPin.EdgeDetected() {
		startTime = time.Now()
	}

	if u.EchoPin.EdgeDetected() {
		endTime = time.Now()
	}

	return endTime.Sub(startTime)
}

//Close closes the RPI io interface
func (u *Ultrasonic) Close() {
	u.EchoPin.Detect(rpio.NoEdge) // disable edge event detection
	rpio.Close()
}
