/*

An example of edge event handling by @Drahoslav7, using the go-rpio library

Waits for button to be pressed twice before exit.

Connect a button between pin 22 and some GND pin.

*/

package main

import (
	"fmt"

	"gitlab.com/balanovic/rpi-sr04/pkg/comms"
)

func main() {

	s := comms.NewUltrasonic(15, 14)
	s.Initialize()

	measurement := s.Measure()
	fmt.Printf("Measurement %d", measurement)

	s.Close()
}
