.PHONY: all dep build clean test coverage coverhtml lint
all: build

lint: ## Lint the files
	@golint -set_exit_status ./...

test: ## Run unittests
	@go test -short ./...

race:  ## Run data race detector
	@go test -race -short ./...

msan:  ## Run memory sanitizer
	@go test -msan -short ./...

coverage: ## Generate global code coverage report
	./scripts/coverage.sh;

coverhtml: ## Generate global code coverage report in HTMLs
	./scripts/coverage.sh html;

build:  ## Build the lambda binary files
	@env GOOS=linux GOARCH=arm GOARM=7  go build -o cmd/srping/srping ./cmd/srping/...

clean: ## Remove previous build
	@go clean ./...

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
